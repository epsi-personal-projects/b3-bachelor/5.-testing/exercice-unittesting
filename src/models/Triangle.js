class Triangle {
    constructor(sideA, sideB, sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }
    
    getPerimeter () {
        return (this.sideA + this.sideB + this.sideC);
    }

    getSideB() {
        return this.sideB;
    }

    getType() {
        if(this.sideA === this.sideB && this.sideB === this.sideC) 
            return "equilateral";

        if(this.sideA === this.sideB || this.getSideA === this.sideC || this.sideB === this.sideC) 
            return "isosceles";

        return "scalene"
    }

    getSemiPerimeter() {
        return (this.sideA + this.sideB + this.sideC) / 2;
    }

    getArea() {
        return Math.sqrt(this.getSemiPerimeter() * (this.getSemiPerimeter() - this.sideA) * (this.getSemiPerimeter() - this.sideB) * (this.getSemiPerimeter() - this.sideC));
    }
}

module.exports = {Triangle};