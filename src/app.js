const Cube = require('../src/models/Cube').Cube;
const Triangle = require('../src/models/Triangle').Triangle;
const Circle = require('../src/models/Circle').Circle;


module.exports = {
    Cube:Cube,
    Triangle:Triangle,
    Circle:Circle
}