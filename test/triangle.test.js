const {Triangle}= require('../src/app');
const expect = require('chai').expect;

describe('Testing the Triangle Functions', function() {
    it('1. The perimiter of the Triangle', function(done) {
        let t1 = new Triangle(15, 15, 15);
        expect(t1.getPerimeter()).to.equal(45);
        done();
    });

    it('2. The sideB of the Triangle', function(done) {
        let t2 = new Triangle(14, 36, 19);
        expect(t2.getSideB()).to.equal(36);
        done();
    });

    it('3. Triangle is Equilateral', function(done) {
        let t3 = new Triangle(30, 30, 30);
        expect(t3.getType()).to.equal("equilateral");
        done();
    });

    it('4. Triangle is Isosceles', function(done) {
        let t4 = new Triangle(30, 30, 15);
        expect(t4.getType()).to.equal("isosceles");
        done();
    });

    it('5. Triangle is Scalene', function(done) {
        let t5 = new Triangle(14, 68, 47);
        expect(t5.getType()).to.equal("scalene");
        done();
    });

    it('6. Triangle\'s Semi-perimeter', function(done) {
        let t6 = new Triangle(100, 100, 100);
        expect(t6.getSemiPerimeter()).to.equal(150);
        done();
    });

    it('7. Triangle\'s area', function(done) {
        let t7 = new Triangle(3, 4, 5);
        expect(t7.getArea()).to.equal(6);
        done();
    });
});