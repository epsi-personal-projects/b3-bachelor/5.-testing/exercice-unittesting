const {Circle}= require('../src/app');
const expect = require('chai').expect;

describe('Testing the Circle Functions', function() {
    it('1. The radius of the circle', function(done) {
        let c1 = new Circle(150);
        expect(c1.getRadius()).to.equal(75);
        done();
    });
});